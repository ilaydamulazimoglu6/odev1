﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ödev1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            int sayi1, sayi2,toplam=0;
            sayi1 = Convert.ToInt32(textBox1.Text);
            sayi2 = Convert.ToInt32(textBox2.Text);
            Random r = new Random();
            int[] sayilaar = new int[comboBox1.SelectedIndex];
            for (int i = 0; i < comboBox1.SelectedIndex; i++)
            {
                int sayilar = r.Next(sayi1, sayi2);
                sayilaar[i] = sayilar;
                secilensayilar.Items.Add(sayilaar[i]);
            }
            //İki tane for'a gerek yoktu bence. Tek form üzerinden yapabiliriz.Düzelt.
            for (int i = 0; i < sayilaar.Length; i++)
            {
                toplam += sayilaar[i];
            }
            label3.Text = "Toplanan Sonuc= " + toplam;
        }
    }
}
